# Project 4: Brevet time calculator with Ajax

This project calculates control times.

## ACP controle times

The application uses a web interface to gather data and display it. It takes total distance, begining time, the desired date, and distance.
You can chose between miles and kilometers. When you click the submit button it stores the times in a mongodb data base, when you click
display it will display all the open and close times you have submited. Once the display button has been clicked it deletes all the
entries in the data base.

## How it works

To find how the calculations are made visit: https://rusa.org/pages/acp-brevet-control-times-calculator
To run examples visit: https://rusa.org/octime_acp.html).

## How to run

The application uses a web interfaced that has been implemented with AJAX and flask. The html page is in the templates folder.
acp_times.py does the calculations, flask_brevets.py does the web routing. The credentials.ini file should be filled in with your name,
git reop, and port. To program is ran through docker, to do this run the run.sh file, it automatically binds to port 5000. 
To view the webpage load port 5000 in your browser.

Author: John Hooft Toomey, jhooftto@uoregon.edu